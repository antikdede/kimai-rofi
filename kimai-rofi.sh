username=<KIMAI LOGIN USERNAME>
password=<KIMAI LOGIN PASSWORD>
KimaiURL="http://<KIMAI_IP>/kimai/core/json.php"

#get apiKey
apiKey=$(curl -s -H "Content-Type: application/json" -X POST -d '{"jsonrpc":"2.0","method":"authenticate","params":["'$username'","'$password'"],"id":"1"}' $KimaiURL | cut -d '"' -f 10)

#check for active/started task
ACTIVEID=$(curl -s -H "Content-Type: application/json" -X POST -d '{"jsonrpc":"2.0","method":"getActiveRecording","params":["'$apiKey'"],"id":1}' $KimaiURL | jq -r '.result.items | map("\(.timeEntryID)->\(.customerName)->\(.projectName)->\(.activityName)") | .[]')

start () {
prj=$(curl -s -H "Content-Type: application/json" -X POST -d '{"jsonrpc":"2.0","method":"getProjects","params":["'$apiKey'"],"id":1}' $KimaiURL | jq -r '.result.items | map("\(.projectID) #\(.name)") | .[]' | rofi -dmenu -i)
prjID=${prj%% *}
prjName=${prj#*#}
if [[ $prjID != "" ]]; then
  TASK=$(curl -s -H "Content-Type: application/json" -X POST -d '{"jsonrpc":"2.0","method":"getTasks","params":["'$apiKey'","'$prjID'"],"id":1}' $KimaiURL | jq -r '.result.items | map("\(.activityID) #\(.name)") | .[]' | rofi -dmenu -i)
    if [[ $TASK != "" ]]; then
      curl -H "Content-Type: application/json" -X POST -d '{"jsonrpc":"2.0","method":"startRecord","params":["'$apiKey'","'$prjID'","'${TASK%% *}'"],"id":1}' $KimaiURL
      #hamster start "${TASK#*#}@$prjName, #"$prjName $(date +"%Y-%m-%d %T")
    fi
fi
}

stop () {
  #hamster stop
  curl -H "Content-Type: application/json" -X POST -d '{"jsonrpc":"2.0","method":"stopRecord","params":["'$apiKey'","'${ACTIVEID%%-*}'"],"id":1}' $KimaiURL
}

if [[ $ACTIVEID == "" ]]; then
  start
else
  echo "stop" | rofi -no-click-to-exit -p "task $ACTIVEID running" -dmenu
  stop
  start
fi
