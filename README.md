# kimai-rofi

Starting/Stopping [Kimai](http://www.kimai.org) tasks with rofi

requires rofi + jq (if using Arch, both in community repo, `sudo pacman -S rofi jq`)

Usage:

- Edit first three lines to match your Kimai setup and run the script
- optionally uncomment hamster start/stop lines (17,23) if you also use hamster time tracker and assign a hotkey to the script
